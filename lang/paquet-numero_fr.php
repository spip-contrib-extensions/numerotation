<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/numerotation.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// N
	'numero_description' => 'Ce plugin permet d’un clic de numéroter/re-numéroter/dé-numéroter tous les articles ou sous-rubriques d’une rubrique.',
	'numero_slogan' => 'Gérer facilement la numérotation des articles et rubriques'
);
