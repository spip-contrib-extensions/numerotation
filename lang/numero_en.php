<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/numero?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// I
	'info_denumeroter_objets' => 'Remove @objets@ numbering',
	'info_denumeroter_rubriques' => 'Remove subsections numbering',
	'info_denumeroter_secteurs' => 'Remove sections numbering',
	'info_numeroter_objets' => 'Re-number @objets@',
	'info_numeroter_rubriques' => 'Re-number subsections',
	'info_numeroter_secteurs' => 'Re-number sections',

	// L
	'label_precedent' => 'Position after', # RELIRE
	'label_precedent_0' => 'First position', # RELIRE

	// T
	'texte_sous_rubriques' => 'Subsections',
	'titre_formulaire_numeroter' => 'Update numbering' # RELIRE
);
